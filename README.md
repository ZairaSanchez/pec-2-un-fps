# PEC 2 - Un FPS

Nota: el uso genérico de "el jugador" se refiere a "la jugadora" o "el jugador".

## FUNCIONAMIENTO

El juego tiene dos niveles: 
- El primer nivel es abierto y hay que matar al dron para que suelte la llave que necesitamos para abrir la puerta que nos da acceso al segundo nivel.
- El segundo nivel transcurre por el interior de unos pasillos y hay que coger dos llaves para abrir la puerta que da fin al nivel y al juego. Para conseguir una de las llaves hay que destruir al robot.

## ESTRUCTURA

- Creación de un escenario (Terrain, Escenario) partido en dos, donde la primera parte es descubierta y la segunda cubierta.
- Creación de una plataforma ascensora que sube y baja continuamente en el primer nivel (PlataformaAscensora, script ControladorAscensor).
- Creación de una zona de estado del jugador, donde se puede ver la cantidad de vida y escudo (EstadoFPS, script EstadoFPS), el arma y la munición (Arma, script ControladorArma).
- Creación de un FPSController y adición de un script para controlar su estado (vida y escudo) (script ControladorJugador).
- Creación de tres armas (pistola, metralleta y escopeta) con diferentes cadencias que el jugador puede elegir con el botón derecho del ratón, el disparo (script ControladorArma), las balas (Bala, script ControladorBala) y los agujeros de bala (AgujeroBalaPrefab).
- Se han añadido objetos que el jugador puede recoger:
	· Botiquines: cuando el jugador recoge un botiquín, su vida y su escudo se rellenan y el botiquín desaparece (script ControladorJugador - OnTriggerEnter).
	· Llaves: cuando el jugador recoge una llave, se muestra en la pantalla y la llave desaparece (script ControladorJugador - OnTriggerEnter).
	· Cajas de munición: cuando el jugador recoge munición, se recarga el arma que lleve en la mano en ese momento (script ControladorArma - Recarga).
- Creación de dos enemigos diferentes que disparan al jugador cuando se acerca (se han modificado los scripts enemyAIDemo, IdleStateDemo y AlertStateDemo, creando los nuevos scripts enemyAI, PatrolState, AlertState y AttackState) y tienen una barra de vida (script EstadoEnemigo). Se ha construido un camino para que patrulle cada uno y dan una vuelta completa si el jugador pasa junto a ellos pero luego se aleja. Al destruirlos, sueltan una llave.
- Creación de un río de lava que rodea el escenario. Si el jugador cae en él, muere.
- Creación de un menú para salir o reiniciar la partida que aparece cuando se muere o se finaliza el juego (script ControladorJuego).
- Creación de sonidos: caminar, saltar, caer, cambiar de arma, disparar, recargar, herir o ser herido, abrir una puerta y coger un objeto (botiquín o llave).
- Creación de puertas automáticas: una se abre automáticamente (se ha modificado el script AutomaticDoors), y las otras dos se abren cuando el jugador tiene en su poder las llaves necesarias (scripts PuertaNivel1 y PuertaNivel2):
	· La de entrada se abre automáticamente al acercarse el jugador, y da al río de lava, con lo que se pierde la vida si se atraviesa.
	· La que da acceso al nivel 2 se abre con una llave que tiene el dron del nivel 1.
	· La que permite finalizar el nivel 2, necesita dos llaves para abrirse.
- Elementos de interfaz:
	· Barras de vida y escudo del jugador.
	· Llaves que tiene en su poder.
	· Arma que lleva en la mano y munición.
	· Menú.

La implementación en detalle se puede seguir a través de los comentarios del código.
